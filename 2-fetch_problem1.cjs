const usersUrl = "https://jsonplaceholder.typicode.com/users"

function fetchData(dataUrl) {
    return new Promise((resolve, reject) => {
        const response = fetch(dataUrl)
        response.then((receivedData) => {
            if (receivedData.status == 200) {
                const data = receivedData.json()
                // console.log(usersdata)
                resolve(data);
            }
            else {
                reject("fail");
            }
        });
    })
}

// 1. Fetch all the users
fetchData(usersUrl)
    .then((response) => {
        console.log(response)
    }).catch((error) => {
        console.log(error)
    });