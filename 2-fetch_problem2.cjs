const todosUrl = "https://jsonplaceholder.typicode.com/todos"

function fetchData(dataUrl) {
    return new Promise((resolve, reject) => {
        const response = fetch(dataUrl)
        response.then((receivedData) => {
            if (receivedData.status == 200) {
                const data = receivedData.json()
                // console.log(usersdata)
                resolve(data);
            }
            else {
                reject("fail");
            }
        });
    })
}

//2. Fetch all the todos
fetchData(todosUrl)
    .then((response) => {
        console.log(response)
    }).catch((error) => {
        console.log(error)
    });